// api url for show all the weather
exports.allCity = function(){
    return 'https://assignmentinteractiveapi.herokuapp.com/all';
}

// api url for show one weather
exports.getCity = function(city){
    return 'https://assignmentinteractiveapi.herokuapp.com/get/' + city;
}

// api url for insert one weather
exports.insertCity = function(city){
    return 'https://assignmentinteractiveapi.herokuapp.com/post/' + city;
}

// api url for update one weather
exports.updateCity = function(city){
    return 'https://assignmentinteractiveapi.herokuapp.com/put/' + city;
}

// api url for delete one weather
exports.deleteCity = function(city){
    return 'https://assignmentinteractiveapi.herokuapp.com/del/' + city;
}